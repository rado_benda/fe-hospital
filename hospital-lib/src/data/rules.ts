export default {
    'F': [
        {
            drugs: ['P', 'As'],
            outcome: 'X',
        },
        {
            drugs: ['As'],
            outcome: 'H',
        },
        {
            drugs: ['P'],
            outcome: 'H',
        }
    ],
    'H': [
        {
            drugs: ['P', 'As'],
            outcome: 'X',
        },
        {
            drugs: ['I', 'An'],
            outcome: 'F',
        },
    ],
    'D': [
        {
            drugs: ['P', 'As'],
            outcome: 'X',
        },
        {
            drugs: ['I'],
            outcome: 'D',
        },
        {
            drugs: ['death'],
            outcome: 'X',
        },
    ],
    'T': [
        {
            drugs: ['P', 'As'],
            outcome: 'X',
        },
        {
            drugs: ['An'],
            outcome: 'H',
        },
    ],
    'P': [
        {
            drugs: ['P', 'As'],
            outcome: 'X',
        },
        {
            drugs: ['Ib'],
            outcome: 'H',
        },
    ],
}
