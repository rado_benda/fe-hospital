import {PatientsRegister} from './patientsRegister';
import {Rule} from './rule';
import treatmentRules from './data/rules';

export class Quarantine {
    private patientsIn: PatientsRegister;
    private patientsOut: PatientsRegister;
    private drugs: Array<string>;
    private readonly rules: any;

    constructor(patients: PatientsRegister) {
        this.rules = treatmentRules;
        this.setPatients(patients);
    }

    public setPatients(newPatients: PatientsRegister): void {
        this.patientsIn = {...newPatients};
        this.patientsOut = {...newPatients};
    }

    public setDrugs(drugs: Array<string>): void {
        this.drugs = [...drugs];
    }

    public wait40Days(): void {
        for (let key in this.patientsIn) {
            if (this.patientsIn.hasOwnProperty(key)) {
                const numberOfSubjects = this.patientsIn[key];

                // As we have in patientsOut reset every state to 0 we don't need to have else condition
                if (numberOfSubjects > 0 && key !== 'X') {
                    const rulesForSubjects = this.rules[key] || [];

                    // Generic for loop is used intentionally as it allows to terminate loop if there is a
                    // change in subject's condition.
                    // Note: In case of D if I is applied this is also considered as change of condition.
                    //       The reason for that is to avoid falling to final 'death' rule.
                    //       It can be done differently however with this approach the whole system is driven by
                    //       conditions and their order. Which allows universal approach (within current requirements).
                    for (let i = 0; i < rulesForSubjects.length; i++) {
                        const rule: Rule = rulesForSubjects[i];
                        const isApplicable: boolean = this.checkDrugsFromRule(rule.drugs, this.drugs);

                        // Check if rule is applicable with provided drugs and if it changes the status of subject.
                        if (isApplicable) {
                            if (key !== rule.outcome) {
                                // Only change numbers if there is status change
                                this.patientsOut[rule.outcome] += numberOfSubjects;
                                this.patientsOut[key] -= numberOfSubjects;
                            }

                            // End the loop.
                            i = rulesForSubjects.length;
                        }
                    }
                }
            }
        }
    }

    public report(): PatientsRegister {
        return this.patientsOut;
    }

    // -------------------------------------------------------------------------
    // Additional private helper method

    private checkDrugsFromRule(ruleArray: Array<string>, drugsArray: Array<string>): boolean {
        // Special check for 'final' death rule. It has precedence over missing drugs as it is applied if no
        // medicine is provided
        if (ruleArray[0] === 'death') {
            return true;
        }

        // If drugs are not provided at all
        if (!drugsArray) {
            return false;
        }

        return ruleArray.every(drug => drugsArray.includes(drug));
    }
}
